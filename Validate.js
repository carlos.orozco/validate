

class Validate{
	constructor(){
		this.invalidData = []
		this.phraseEmail = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i
		this.phraseNumber = /[0-9]/
		this.phraseString = /^[a-zA-Z]*$/
		this.listInvalidData = []
		this.requiredGenerel = false
	}

	test(props){
		let data = props.data, length = data.length, i=0, current = false;
		// this.clearError()
		this.listInvalidData = []
		this.invalidData = []
		this.isRequired(props)
		for(i; i< length; i++){
			!data[i].hasOwnProperty('required') ? data[i]["required"] = this.requiredGenerel : false; 
			data[i].hasOwnProperty('field') ? this.fieldValidate(data[i]) : this.fieldsValidate(data[i])
		}
		return this.renderData()
	}

	fieldValidate(props) {
		!props.hasOwnProperty("value") ? props["value"] = undefined : false;
		!props.hasOwnProperty("multiData") ? props["multiData"] = false : false;
		if(props.required){
			this.valRequired(props) 
			props.hasOwnProperty('valString') ? this.valString(props) : false;
			props.hasOwnProperty('valNumber') ? this.valNumber(props) : false;
			props.hasOwnProperty('regularPhrase') ? this.valRegularPhrase(props) : false;
			props.hasOwnProperty('maxLength') ? this.valMaxlength(props) : false;
			props.hasOwnProperty('minLength') ?  this.valMinLength(props) : false;
			props.hasOwnProperty('email') ?  this.valEmail(props) : false;
			props.hasOwnProperty('emailAndText') ?  this.valEmailAndText(props) : false;
		}
	}

	fieldsValidate(props) {
		let i=0, data = document.querySelectorAll(props.fields), length = data.length, valueCurrent; 
		if(props.required){
			for(i; i < length; i++){
				console.log(i)
				props["value"] = data[i].value;
				props["multiData"] = true;
				props["index"] = i;
				this.fieldValidate(props)
			}
		}
	}

	isRequired(props){
		if(props.hasOwnProperty("general")) {
			if(props.general.hasOwnProperty("required")){
				this.requiredGenerel = props.general.required === true ? true : false;
			}
		}
	}

	renderData(){
		if(this.invalidData.length > 0){
		    this.markError();
		    return false;
		}else{
		    return true;
		}
	}

	/*valid requerid*/
	valRequired(props) {
		let boolData, data = props.value == undefined ? document.querySelector(props.field).value : props.value;
		if(props.required){
			boolData = data.length > 0 ? false : true;
			if(boolData){
				if(props.multiData == true){
					this.pushData(props, "Valor requerido") 
				}
				else{
					!this.listInvalidData.includes(props.field) ? this.pushData(props, "Valor requerido") : false;
				}
			}
		}
	}

	/*valid string*/
	valString(props){
		let boolData, data = props.value == undefined ? document.querySelector(props.field).value :props.value;
		boolData = !this.phraseString.test(data) ? true : false;
		if(boolData){
			!this.listInvalidData.includes(props.field) ? this.pushData(props, "Invalido requiere solo string") : false; 
		}
	}

	/*valid number*/
	valNumber(props){
		let boolData, data = props.value == undefined ? document.querySelector(props.field).value :props.value;
		boolData = !this.phraseNumber.test(data) ? true : false;
		if(boolData){
			!this.listInvalidData.includes(props.field) ? this.pushData(props, "Invalido requiere solo numero") : false
		}
	}

	/*valid regular expressions*/
	valRegularPhrase(props){
		let boolData, data = props.value == undefined ? document.querySelector(props.field).value :props.value;
 	    boolData = !props.regularPhrase.test(data) ? true : false;
		if(boolData){
			!this.listInvalidData.includes(props.field) ? this.pushData(props, "Expresion regular invalida") : false
		} 
	}

	/*valid min length*/
	valMinLength(props){
		let boolData, data = props.value == undefined ? document.querySelector(props.field).value :props.value;
	    boolData = props.minLength > data.length ? true : false;
		if(boolData){
			!this.listInvalidData.includes(props.field) ? this.pushData(props, "Invalida en minima longitud") : false
		}
	}

	/*valid max length*/
	valMaxlength(props){
		let boolData, data = props.value == undefined ? document.querySelector(props.field).value :props.value;
	    boolData = props.maxLength < data.length ? true : false;
		if(boolData){
			!this.listInvalidData.includes(props.field) ? this.pushData(props, "Invalida en maxima longitud") : false
		}
	}

	/*valid mail*/
	valEmail(props){
		if(props.email) {
			const boolData = this.valRegularPhrase({
					field : props.field,
					regularPhrase: this.phraseEmail
			})
		} 
	}

	/*valid if it is mail or text*/	
	valEmailAndText(props){
		const data = document.querySelector(props.field).value
		let boolDataEmail = undefined,  boolDataString = undefined;
		if(props.emailAndText) {
			boolDataEmail  = !this.phraseEmail.test(data) ? true : false
			if(boolDataEmail) {
				boolDataEmail = false
				boolDataString = !this.phraseString.test(data) ? true: false
			}
			if(boolDataEmail || boolDataString){
				!this.listInvalidData.includes(props.field) ? this.pushData(props, "Invalido solo correo o texto") : false;
			}
		}
	}

	markError(){
        let i=0, length = this.invalidData.length, doc = document;
        for(i; i < length; i++){
        	console.log(this.invalidData)
        	console.log(this.listInvalidData)

        	// let span = doc.createElement("span")
        	// span.innerHTML = this.invalidData[i].error
        	// span.className = 'validateError'
        	// let parent = doc.querySelector(this.invalidData[i].field).parentNode
        	// parent.appendChild(span)
        }
	}

	fieldMark(){

	}

	fieldsMark(){

	}

	pushData(props, error){
		props.hasOwnProperty('error') ? props.error : error;
		this.invalidData.push(props)
		this.listInvalidData.push(props.multiData == true ? props.fields : props.field )
	}

	clearError(){
		const x = document.querySelectorAll('.validateError').removeElement()
	}

}
